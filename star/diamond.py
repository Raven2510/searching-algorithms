def main(size):
	i = 1
	
	while i <= size:
		j = 1
		while j < (size * 2):
			if (j == size) or (j >= size - i + 1 and j <= size + i - 1):
				print('*', end='')
			else:
				print(' ', end='')
			j += 1
		print()
		i += 1
	
	i = size - 1
	while i > 0:
		j = size * 2 - 1
		while j > 0:
			if (j == size) or (j >= size - i + 1 and j <= size + i - 1):
				print('*', end='')
			else:
				print(' ', end='')
			j -= 1
		print()
		i -= 1

	
main(5)