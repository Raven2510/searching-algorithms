items = [7, 9, 13, 19, 39, 40, 61, 81, 84, 87]
target = 39
n = len(items)

#loop implementation
def linearSearchLoop(items, target, n):
    for item in items:
        if item == target:
            return items.index(item)
    return None

print("(Loop) index: " + str(linearSearchLoop(items, target, n)))

#recursive implementation
def linearSearchRecursion(items, target, n, i = 0):
    if items[i] == items[n] and items[i] != target:
        return None
    elif items[i] == target:
        return i
    else:
        return linearSearchRecursion(items, target, n, i + 1)

print("(Recursive) index: "+str(linearSearchRecursion(items, target, n - 1)))
